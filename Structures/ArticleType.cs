﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArticleTypes.Structures
{
    public enum Type
    {
        Vetement, Alimentaire, Loisirs
    }
   public class ArticleType
    {
        protected string Designation { get; set; }
        protected int Prix { get; set; }
        protected int Quantite { get; set; }
        protected Type Type { get; set; }

        public void Afficher()
        {
            Console.WriteLine(this.Designation);
            Console.WriteLine(this.Prix);
            Console.WriteLine(this.Quantite);
            Console.WriteLine(this.Type);
        }

        public void Ajouter(int integer)
        {
            this.Prix = this.Prix + integer;
        }

        public void Retirer(int integer)
        {
            this.Prix = this.Prix - integer;
        }

        public ArticleType (string designation, int prix, int quantite, Type type)
        {
            this.Designation = designation;
            this.Prix = prix;
            this.Quantite = quantite;
            this.Type = type;
        }
    }
}

