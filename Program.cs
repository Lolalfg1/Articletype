﻿using ArticleTypes.Structures;
using System;

namespace ArticleTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            var a1 = new ArticleType("stp", 2, 2, Structures.Type.Alimentaire);

            a1.Afficher();
            a1.Ajouter(20);
            a1.Retirer(10);
            a1.Afficher();

        }
    }
}
